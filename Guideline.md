---
title: Open Source Hardware Guideline | Business Models
license: CC-BY-SA-4.0
date: Jan 11th 2022
lang: en-US
charset: UTF-8
created by: Jonah Schuldt, Luca Klein, Korbinian Kuhn, Jennifer-May Hoffmann
papersize: a4
geometry: "top=2cm,bottom=2cm,left=3cm,right=3cm"
...


# Open Source Hardware Guideline | Business Models

![](https://md.opensourceecology.de/uploads/upload_3d879f7354074f35d7e5702dc70c7623.png)

# Foreword 

This guide is a summary of Open Source Hardware and gives an overview of the possibilities that open source hardware offers.\
[Open Source Hardware](https://www.oshwa.org/definition/) is hardware whose design is made publicly available so that anyone can study, modify, distribute, make and sell the design or hardware based on that design. 

In the following, there will be an overview of how companies have used OSH, why it should be used and how money can be made from it.

# Introduction 

With open-source hardware, as with software, it is a question of using the construction plans. Whereas the respective blueprint for hardware still has to be implemented and a finished product is already created in the software.\
In practice a company has to bear the following costs for these points when developing products:

- development 
- production 
- repair 
- service 
- adaptations 
- documentation 
- recycling
- reprocessing 
- workshops

Development and documentation are the biggest challenges, which would already be given by OSH. Open-Source Hardware (OSH) is a term for tangible objects - machines, devices or other physical things - whose design has been made available to the public in such a way that anyone can make, modify, distribute and use them. 

Hardware differs from software in that physical resources must always be used to produce physical goods. Accordingly, persons or companies producing items ("products") must do so under an OSHW license. They must make clear that such products are not manufactured, sold, warranted, or otherwise sanctioned by the original developer. No trademarks belonging to the original developer may be used.
 
# Examples 

To get an impression of the possibilities, the following will deal with four large companies that have become successful with open source hardware.


## Prusa

Prusa Research was founded in 2012 as a one-man startup by Josef Prusa, a Czech hobbyist, maker and inventor - and now one of the best-known names in the 3D printing industry. Over time, the Prusa i3 design became one of the most popular FDM 3D printer designs, adopted by many makers and hobbyists around the world thanks to its open-source nature.

There are currently more than 600 people working in Prusa Research and they ship over 9000 printers every month directly from their headquarters in Prague all over the world. . Prusa's annual turnover was $72.6 million in 2019. They are the fastest-growing technology company in Central Europe ([Deloitte 2018](https://www2.deloitte.com/cz/en/pages/press/articles/cze-tz-ce-fast-50-nejrychleji-rostl-cesky-vyrobce-3d-tiskaren-prusa-research.html)) and ranked first in Deloitte 2019's BIG 5 category.

From the beginning, Pursa has published all its blueprints for all new and further developments. With its strong brand and knowledge, it has nevertheless been able to build such a fast-growing company.
 
## Arduino

Arduino is an open-source electronics platform based on easy-to-use hardware and software that had annual sales of $161.9 million in 2019. Arduino boards are able to read input - light on a sensor, a finger on a button, or a Twitter message - and convert it into an output - activate a motor, turn on an LED. The Arduino board and software are open source and evolve with contributions from users around the world.

Because of their reputation and what the brand stands for, Arduino is able to sell their boxes at high price, even though that the blueprints have been published.
   
## Sparkfun

Sparkfun, with annual sales of $72.6 million in 2019, makes DIY electronics products and helps its community with knowledge by creating courses and tutorials.

Sparkfun's products and resources are not patented, but published under a free license, so they can be used, edited and also sold by anyone. The company also benefits from constantly innovating.

But the primary way for them to make money is to come up with new product ideas and sell those on their website.

## Open Source Ecology

Open Source Ecology is a network of farmers, engineers, and other supporters who produce agricultural machinery that meets the conditions for free hardware. They develop open-source machines that can be made at a fraction of the commercial cost, and make their designs available online for free. The goal of Open-Source Ecology is to create an open-source economy - an efficient economy that fosters innovation through open collaboration. 
 
One way to fund products is through workshops that focus on creating powerful experiences. Workshops can be a key activity of your open hardware company. Participants pay to attend the workshop and learn how to build the product. For 90 days, you can work with a team and build things that will blow your mind. 

## Adafruit Industries

The owner of Adafruit is Limor Fried, she studied electrical engineering and computer science and started selling electronic kits with her designs on her website. In 2005, she founded the company Adafruits Industries. In 2016, the company had 105 employees and $45 million in revenue. 

Adafruit Industries sells, designs, and manufactures electronic products and components. They also provide learning resources, including live and recorded videos about electronics and programming for anyone who wants to learn about electronics or technology in general. 
 
## OpenROV

OpenROV was founded by Eric Stackpole and David Lang as an OSH project, startup, and DIY community. 

OpenROV is a low-cost telerobotic underwater drone designed to make underwater research and education affordable. The company demonstrates how open-source can be applied in different areas.

# [Motivation](https://www.researchgate.net/publication/316884384_Why_Open_Source_Exploring_the_Motivations_of_Using_an_Open_Model_for_Hardware_Development)

When setting up a project or a company, the decision to use open-source hardware can come from various motivations. These can be divided into intrinsic and extrinsic reasons.\
Intrinsic motivators have an idealistic character. This can be aimed at sustainability in ecological, social or even economic terms. Intrinsic motivators could be:

-	Personal satisfaction
-	Altruism
-	Hacker Ethic 
-	Reciprocity 

Recognition and the realization of one's dreams, through the publication of designs can be summarized under personal satisfaction.

Altruism refers to the creation of products with social benefits. Problems that an individual had can be simplified with the help of created products and should be made accessible to as many as possible.

The general principles of the hacker ethic are sharing, free information, practical constraint, and community [Steven Levy]. 
Design should be shareable and freely accessible.

Founders who have already benefited a lot from the open source community want to give back value and contribute to scale up (Reciprocity).

Extrinsic motivators include the economic benefits of using open-source hardware. These are also partially addressed again in the funding opportunities. However, these also include:

-	Technological
-	Economical
-	Product-based

Open source provides the ability to access technical knowledge to. 

1. rapid and cost-effective product development is possible through the additional gain of idea and knowledge-based technology resources. 
2. demonstration of technological capabilities, to attract more clients. 
3. establishing technology standards that enable market dominance.

Economically, open source helps reduce costs and increase profits. 
Costs can be lowered by 

1. lowering R&D costs, 
2. lowering recruitment costs, 
3. eliminating patenting costs, through the license model.

On the other hand, profits can be increased by

1. building a platform with products, community, forums, documentation, tutorials etc. and 
2. providing related services such as coaching, consulting or incubating.

Some products are difficult to sell in the normal market through their users or distribution permission. Therefore, OSH is useful in this context. 

1. customer demand: the most important customers only use products that are open source.
2. distribution permission: distribution policies for certain types of products, such as medical devices, make companies publish their design to allow researchers to adapt them to their purposes.

# Financing options 

The financing of projects or whole companies is the biggest challenge because the costs for development and documentation are not rewarded with a monopoly position.
In this guideline are 9 common possibilities given, but there are many more.
surcharge on products and services

**1.	Special know-how** 

Knowledge about specific OSH is sold, such as support, training, consulting and technical assistance.\
RedHat sells services around Linux. Features for Linux are offered, or individual adjustments are made.

**2.	Strong brands**

A strong brand makes it possible to sell products at a higher price.\
Arduino produces microcontrollers. The Construction plan is open source and can be legally produced by other manufacturers. But because of the strong brand of Arduino, they can still sell the same product for a higher price.

## Commercial orders with open source under certain conditions

Companies have found that ‘going open’ does not contradict being commercial. A clear benefit to the companies in the use and re-use of existing designs or the customization of designs for a specific customer, thereby effectively reducing design costs.\
If a company has the appropriate production facilities, it is possible to include any design licensed under an open hardware license in its product portfolio. By releasing their products under an open hardware license, companies also benefit from peer review, which leads to improved products. They are more closely aligned with end-user requirements.  If someone makes a new design, change or bug fix for a particular piece of hardware, that new design is released under the same terms. That is the case, if the original hardware was released under a strong copyleft license. It would not be the case, if it was released under a permissive license.\
For example, 'CERN' is one of the largest centers for scientific research. The organization's focus is on innovation and benefits for society. At CERN, OSH is used for the timing system of the antiproton accelerator, for example. CERN hosts it's own GitLab instance for OSH (ohwr.org) and created the first and yet best maintained OSH licenses.

## Open Core

Companies make their core product freely available. Extensions are available for purchase at an additional cost.\
This could also be seen as a freemium model. This is nowadays also a popular business model in many non-open-source applications.

[GitLab](https://about.gitlab.com/pricing/) is an example for Open Core Project in Open Source Software and is an open-source web application based on Git.\ With it, you can easily develop, maintain, document your code and use many other handy tools. To use GitLab in a team a (free) membership is required.
 
## Memberships 

A paid membership finances the OSH.\
This is a model that is already used in the open source software sector and is transferable to hardware. For example by giving special offers and selling the product itself for the material cost.

FreeCAD for Example is a BIM software for architects. You can do a wide range of simulations, such as calculating the effort in structures, energy consumption, etc. with BIM software for free. Users in this community can support this project by donating on a Patreon page. 

## Crowdfunding

Crowdfunding is the practice of funding a project or venture by raising money from a large number of people whom each contribute a relatively small amount, typically via the internet.

The most popular and largest crowdfunding platforms are Kickstarter and Startnext. Another one specifically for open-source-projects is “open collective”, but there are plenty more crowdfunding platforms. However, you don't necessarily have to finance the project via one of these platforms, but can also generate enough attention for funding via your reach.

One example is Brewdog. They are an open source brewery. The open source refers to the recipes. It is financed via crowdfunding and the donors receive many small benefits like free delivery or 10% off. 

## Public funding 

Generally, funding is provided through calls for proposals or within the framework of research projects. 

In the US, this is mainly done through the Shuttleworth Foundation & Sloan Foundation. NLnet is working in Europe, which financially supports organizations and people that contribute to an open information society.

And Open Knowledge Foundation (DE) is just setting up the first Open Hardware Fund, which is special, because a registered organization is not required for an application. Hence individuals and small groups can also get funded. It is the only grant of that kind in Germany.

## Shared costs

Several companies and organizations join forces to find a solution to a problem. They all have the same problem, but only have to bear a fraction of the development costs.

The Open Compute Project (OCP) is an organization that shares designs of data center products and best practices among companies, including ASUS, ARM, Facebook, IBM and many more. Facebook was the founder of OCP. When they were scaling hard in 2009 they needed reliable, scalable infrastructure at lowest possible costs and that’s why they founded OCP.

OCP aims to build an open source hardware ecosystem for the data center market. It leverages the principles of open source collaboration. There are four levels of membership (Community, Silver, Gold, Platinum). The levels show the community how high an organization's contribution requirements are. The higher the level of membership, the higher the contribution requirements, but the lower the cost. 

## Voluntary cooperation

An active community that is actively promoted makes it possible to drastically reduce development costs. This is because the community is allowed to participate in development voluntarily. 

Farmbot for Example is an OSH manufacturer for the garden. The robot waters and plants a bed fully automatically. Since the documentation for the project is completely published, anyone can think up and implement special extra applications. This way, developments from the community can also push the product further.

## Pay per Bug 

This model also never completely finances a project, but it can help to reduce development costs. Instead of support, the community is encouraged to find bugs. If a bug is found and fixed, a reward is given. 

This is currently a common software model. An example is Nextcloud. If someone points out a security flaw to Netxcloud, they will receive a reward, depending on the security risk. This Process could also be used in the hardware sector, where you get a Reward for errors in the construction plans.

## Advertising 

A small preface to the use of advertising:

In the open source community, one should think about whether advertising could disrupt the community. After all, the community is the most important thing in the open source world. However, if you can create a win-win situation for the community and the advertiser, then advertising can be a suitable means of funding. 

There are several ways to earn money through advertising: 

**1. Refer to others**

For example, the documentation can refer directly to parts manufacturers who pay you a commission or advertising fee. (Building material manufacturers, electronic accessory stores, etc.) 

**2. Logos of others with on the construction plans pack** 

It would also be conceivable to include the logos of other companies in the blueprints. These companies would then pay you money for advertising on them. Every user should be free to remove the logos if he wants to use this hardware. This is however connected with an additional expenditure. It is also important to make sure that it is used in the right dosage and positioning so that the idea behind open source hardware is not lost.

**3. As a PR stunt**

Example: Tesla caused a stir in the media by publishing their patents under their license. However, this prevents other car manufacturers from reasonably using Tesla's technology. Elon Musk's Hyperloop project could also be chalked up as a successful PR stunt. The plans for this are available open source and Elon Musk is now inextricably linked to this project, even if he has given this project away. In any case, this has played strongly on his image and brought his other companies further. 

# Challenges 

As OSH is still such a new area in economic terms, there are many questions have not yet been clarified.\
It is a new type of development that has to be integrated into existing systems and completely new tasks arise, as well as legal questions that have to be clarified for the first time. This requires courageous people who go down this path and turn an experimental path into a new form of enterprise. Who find a way to deal with the independence of customers with certain companies.   

# Selection of the OSH

To give an overview of existing projects and the available licenses, a database has been set up under which these are collected.

<losh.opennext.eu>
 
# Licensing

Copyright does not apply to hardware in the same way as it does to software (see details in the [legal issues guideline](https://osegermany.gitlab.io/osh-guideline-legal-issues/Guideline.pdf) or in this [tl;dr](https://github.com/OPEN-NEXT/tldr-ipr/releases/download/v1.0.0/tldr-IPR.pdf)). It should be noted that useful, as well as functional objects, are excluded from copyright protection. The design files are protected by copyright, but the hardware itself cannot be protected by a license.\
One way to protect the rights to a functional object is through patents. Unfortunately, patents are expensive and time-consuming to acquire. 
It is nevertheless advisable to apply for a license, as it clarifies what the OSH framework is.

Over-simply speaking, there are two important types of licencing schemes you can choose between (however, there are many more):

-	Copyleft license
-	Permissive license

Copyleft licenses require that the derivative products have to be released under the same licenses as the original. Common copyleft licenses are for example: GNU General Public License (GPL), Creative Commons Attribution-ShareAlike license, CERN Open Hardware License (OHL), TAPR Open Hardware License.

The permissive license allows that the derivatives from the original do not have to be released under the same license as the original. Common permissive licenses are for example: FreeBSD license, MIT license, Creative Commons Attribution license.\
A third possibility is a public domain. You have all the rights but no obligations. This is less a license than a marking (geek note: [CC0](https://creativecommons.org/share-your-work/public-domain/cc0/) provides a workaround e.g. for France and Germany where copyright cannot be waived in order to make something available under public domain).

This list does not claim to be exhaustive, but only gives an initial overview of the most common licensing methods used. For certainty, one should seek advice from a lawyer in the field.

# Policies and Rules

These rules have been created by the Open Source Hardware Association but are not to be followed like laws.

The signatories of this Open Source Hardware definition recognize that the open source movement represents only one way of sharing information. They encourage and support all forms of openness and collaboration, whether or not they meet this definition.

**1. Documentation**

The hardware must be released with documentation including design files, and must allow modification and distribution of the design files. Where documentation is not furnished with the physical product, there must be a well-publicized means of obtaining this documentation for no more than a reasonable reproduction cost, preferably downloading via the Internet without charge. The documentation must include design files in the preferred format for making changes, for example the native file format of a CAD program. Deliberately obfuscated design files are not allowed. Intermediate forms analogous to compiled computer code — such as printer-ready copper artwork from a CAD program — are not allowed as substitutes. The license may require that the design files are provided in fully-documented, open format(s).

**2. Scope**

The documentation for the hardware must clearly specify what portion of the design, if not all, is being released under the license.

**3. Necessary Software**

If the licensed design requires software, embedded or otherwise, to operate properly and fulfill its essential functions, then the license may require that one of the following conditions are met:

1.  The interfaces are sufficiently documented such that it could reasonably be considered straightforward to write open source software that allows the device to operate properly and fulfill its essential functions. For example, this may include the use of detailed signal timing diagrams or pseudocode to clearly illustrate the interface in operation.
2. The necessary software is released under an OSI-approved open source license.

**4. Derived Works**

The license shall allow modifications and derived works, and shall allow them to be distributed under the same terms as the license of the original work. The license shall allow for the manufacture, sale, distribution, and use of products created from the design files, the design files themselves, and derivatives thereof.

**5. Free redistribution**

The license shall not restrict any party from selling or giving away the project documentation. The license shall not require a royalty or other fee for such sale. The license shall not require any royalty or fee related to the sale of derived works.

**6. Attribution**

The license may require derived documents, and copyright notices associated with devices, to provide attribution to the licensors when distributing design files, manufactured products, and/or derivatives thereof. The license may require that this information be accessible to the end-user using the device normally, but shall not specify a specific format of display. The license may require derived works to carry a different name or version number from the original design.

**7. No Discrimination Against Persons or Groups**

The license must not discriminate against any person or group of persons.

**8. No Discrimination Against Fields of Endeavor**

The license must not restrict anyone from making use of the work (including manufactured hardware) in a specific field of endeavor. For example, it must not restrict the hardware from being used in a business, or from being used in nuclear research.

**9. Distribution of License**

The rights granted by the license must apply to all to whom the work is redistributed without the need for execution of an additional license by those parties.

**10. License Must Not Be Specific to a Product**

The rights granted by the license must not depend on the licensed work being part of a particular product. If a portion is extracted from a work and used or distributed within the terms of the license, all parties to whom that work is redistributed should have the same rights as those that are granted for the original work.

**11. License Must Not Restrict Other Hardware or Software**

The license must not place restrictions on other items that are aggregated with the licensed work but not derivative of it. For example, the license must not insist that all other hardware sold with the licensed item be open source, nor that only open source software be used external to the device.

**12. License Must Be Technology-Neutral**

No provision of the license may be predicated on any individual technology, specific part or component, material, or style of interface or use thereof.

--

Source: <https://www.oshwa.org/definition/>

# Legal aspects

The following link shows legal aspects to open-source hardware.\
<https://gitlab.com/OSEGermany/osh-guideline-legal-issues>

**In summary, OSH has great potential, and if you become part of it, the community will grow and so will the progress.**

# Contacts

Martin Häuer – OSEGermany e.V., Fraunhofer-IPK\
<https://de.linkedin.com/in/martin-häuer-16a588208>\
martin.haeuer@ipk.fraunhofer.de

Jaime Arredondo – Bold & Open\
<https://boldandopen.com>\
jaime@boldandopen.com

Lars Zimmermann – Mifactory\
https://larszimmermann.de

Frederik Lean Hansen – Danish Design Center\
<https://ddc.dk/people/frederik-lean-hansen/>\
flh@ddc.dk
