---
type: slide
slideOptions:
  transition: slide
...

<img src="https://www.oshwa.org/wp-content/uploads/2014/03/oshw-logo.svg" style="border: none;background: none;box-shadow:none" height="600">

---

# [Open Source Hardware Guideline](https://md.opensourceecology.de/s/lC5boQ-MA)
## Business Models

---

## [Foreword](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#Foreword) 

This guide is a summary of Open Source Hardware and gives an overview of the possibilities that open source hardware offers.

There is a longread with more information behind each [link](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view).

---

## [Introduction](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#Introduction) 

Open Source Hardware is hardware whose design is made publicly available so that anyone can study, modify, distribute, make and sell the design or hardware based on that design. 

In the following, there will be an overview of how companies have used OSH, why it should be used and how money can be made from it.

---

## [Examples](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#Examples)

To get an impression of the possibilities, the following will deal with six large companies that have become successful with open source hardware.

----

### [**1. Prusa**](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#1-Prusa)

Prusa Research was founded in 2012 as a one-man startup by Josef Prusa.
Over time, the Prusa i3 design became one of the most popular FDM 3D printer designs.
There are currently more than 600 people working in Prusa Research and they ship over 9000 printers every month.

----

### [**2. Arduino**](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#2-Arduino)

Arduino is an open-source electronics platform based on easy-to-use hardware and software that had annual sales of $161.9 million in 2019. The Arduino board and software are open source and evolve with contributions from users around the world.
Because of their reputation and what the brand stands for, Arduino is able to sell their boxes at higher prices, even though that the blueprints have been published.

----

### [**3. Sparkfun**](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#3-Sparkfun)

Sparkfun, with annual sales of $72.6 million in 2019, makes DIY electronics products.
Sparkfun's products and resources are not patented, so they can be used, edited and also sold by anyone. Open source code encourages people to share and learn from each other. The company also benefits from constantly innovating.
But the primary way for them to make money is to come up with new product ideas and sell those on their website.

----

### [**4. Open Source Ecology**](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#4-Open-Source-Ecology)

Open Source Ecology is a network of farmers, engineers, and other supporters who produce agricultural machinery that meets the conditions for free hardware. They develop open-source machines that can be made at a fraction of the commercial cost, and make their designs available online for free. The goal of Open-Source Ecology is to create an open-source economy - an efficient economy that fosters innovation through open collaboration. 

----

### [**5. Adafruit Industries**](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#5-Adafruit-Industries)

The owner of Adafruit is Limor Fried, she started selling electronic kits with her designs on her website. In 2005, she founded the company Adafruits Industries. In 2016, the company had 105 employees and $45 million in revenue. 
Adafruit Industries sells, designs, and manufactures electronic products and components. They also provide learning resources, including live and recorded videos about electronics and programming for anyone who wants to learn about electronics or technology in general. 

----

### [**6. OpenROV**](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#6-OpenROV)

OpenROV was founded by Eric Stackpole and David Lang as an OSH project, startup, and DIY community. 
OpenROV is a low-cost telerobotic underwater drone designed to make underwater research and education affordable. The company demonstrates how differently open-source can be applied. 

---

## [Motivation](https://www.researchgate.net/publication/316884384_Why_Open_Source_Exploring_the_Motivations_of_Using_an_Open_Model_for_Hardware_Development)
When setting up a project or a company, the decision to use open-source hardware can come from various motivations. These can be divided into intrinsic and extrinsic reasons.
Intrinsic motivators have an idealistic character. This can be aimed at sustainability in ecological, social or even economic terms. 

----

## [Intrinsic motivators:](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#Motivation)
Intrinsic motivators could be:
-	Personal satisfaction
-	Altruism
-	Hacker Ethic
-	Reciprocity

----

## [Extrinsic motivators:](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#Motivation)

Extrinsic motivators could be:
-	 Technological
-    Economical
-    Product-based

---

# [Financing options](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#Financing-options) 

The financing of projects or whole companies is the biggest challenge because the costs for development and documentation are not rewarded with a monopoly position.
In this guideline are 9 common possibilities given, but there are many more.

----

### [**1.	Special know-how**](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#Financing-options)
Knowledge about specific OSH is sold, such as support, training, consulting and technical assistance.
RedHat sells services around Linux. Features for Linux are offered, or individual adjustments are made.

----

### [**2.	Strong brands**](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#Financing-options)
A strong brand makes it possible to sell products at a higher price. 
Arduino produces microcontrollers. The Construction plan is open source and can be legally produced by other manufacturers. But because of the strong brand of Arduino, they can still sell the same product for a higher price.

----

### [Pay per Bug](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#Pay-per-Bug)

The model also never completely finances a project, but it can help reducing development costs. Instead of support, the community is encouraged to find bugs. If a bug is found and fixed, a reward is given. 
An example is Nextcloud. If someone points out a security flaw to Netxcloud, they will receive a reward, depending on the security risk. This Process could also be used in the hardware sector, where you get a Reward for errors in the construction plans.

----

### [Voluntary cooperation](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#Voluntary-cooperation)

An active community that is actively promoted makes it possible to drastically reduce development costs. However, this is obviously not financing a project.
Example: Farmbot. The robot waters and plants a bed fully automatically. Since the documentation for the project is completely published, anyone can think up and implement special extra applications. This way, developments from the community can also push the product further.

----

### [Crowdfunding](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#Crowdfunding)

Crowdfunding is the practice of funding a project or venture by raising money from a large number of people whom each contribute a relatively small amount, typically via the internet.
The most popular and largest crowdfunding platforms are Kickstarter and Startnetx. However, you don't necessarily have to finance the project via one of these platforms, but can also generate enough attention for funding via your reach.

----

### [Shared costs](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#Shared-costs)

Several companies and organizations join forces to find a solution to a problem. They all have the same problem, but only have to bear a fraction of the development costs.
The Open Compute Project (OCP) is an organization that shares designs of data center products and best practices among companies, including ASUS, ARM, Facebook, IBM and many more. 
OCP aims to build an open source hardware ecosystem for the data center market.

----

### [Commercial orders with open source under certain conditions](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#Commercial-orders-with-open-source-under-certain-conditions)

OSH development is an evolving niche since meaningful technical documentation and knowledge in licensing schemes are rare qualities. Costumers avoid a vendor lock-in when ordering open source products and may also benefit from peer-reviews and contributions from the community.
For example, at CERN, one of the largest scientific research centers, OSH is widely used, e.g. for the timing system of the antiproton accelerator.

----

### [Open Core](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#Open-Core)

Companies make their core product freely available. Extensions are available for purchase at an additional cost.
This could also be seen as a freemium model. This is nowadays also a popular business model in many non-open-source applications.
‘GitLab’ is an example for Open Core Project in Open Source Software and is an open-source web application based on Git. With it, you can easily develop, maintain, document your code. To use GitLab in a team a membership is required.

----

### [Memberships](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#Memberships)

A paid membership finances the OSH. 
This is a model that is already used in the open source software sector and is transferable to hardware. For example by giving special offers and selling the product itself for the material cost. 
FreeCAD for Example is a BIM software for architects. You can do a wide range of simulations with BIM software for free. Users in this community can support this project by donating on a Patreon page. 

----

### [Subsidies](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#Subsidies)

Generally, funding is provided through calls for proposals or within the framework of research projects. 
In the US, this is mainly done through the [Shuttleworth Foundation](https://shuttleworthfoundation.org/) & [Sloan Foundation](https://sloan.org/). [NLnet](https://nlnet.nl/) is working in Europe, which financially supports organizations and people that contribute to an open information society.
And [Open Knowledge Foundation (DE)](https://okfn.org/) is just setting up the first Open Hardware Fund.

----

### [Advertising](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#Advertising) 

A small preface to the use of advertising:
Advertising is an unpopular and mostly hated form of funding. In the open source community, you should think carefully about whether advertising could upset the community. Because the community is the most important part in the open source world. However, if you can create a win-win situation for the community and the advertiser, then advertising can be a suitable means of financing.
There are several ways for this: 

----

### **1. Refer to others** 

For example, the documentation can refer directly to parts manufacturers who pay you a commission or advertising fee. (Building material manufacturers, electronic accessory stores, etc.) 

----

### **2. Logos of others with on the construction plans pack** 

It would also be conceivable to include the logos of other companies in the blueprints. These companies would then pay you money for advertising on them. Every user should be free to remove the logos if he wants to use this hardware. This is however connected with an additional expenditure. It is also important to make sure that it is used in the right dosage and positioning so that the idea behind open source hardware is not lost. 

----

### **3. As a PR stunt** 

Example: Tesla caused a stir in the media by publishing their patents under their license. However, this prevents other car manufacturers from reasonably using Tesla's technology. 
Elon Musk's Hyperloop project could also be chalked up as a successful PR stunt. The plans for this are available open source and Elon Musk is now inextricably linked to this project, even if he has given this project away. In any case, this has played strongly on his image and brought his other companies further. 

---

## [Challenges](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#Challenges)

As OSH is still such a new area in economic terms, there are many questions have not yet been clarified. 
It is a new type of development that has to be integrated into existing systems and completely new tasks arise, as well as new legal concerns. This requires courageous people who go down this path and turn an experimental path into a new form of enterprise. Who find a way to deal with the independence of customers with certain companies. 

---

## [Selection of the OSH](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#Selection-of-the-OSH)

To give an overview of existing projects and the available licenses, a database has been set up under which these are collected.
losh.opennext.eu

---

## [Licensing](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#Applying-for-a-license)

Copyright only partly applies for hardware (see details in the [legal issues guideline](https://osegermany.gitlab.io/osh-guideline-legal-issues/Guideline.pdf) or in this [tl;dr](https://github.com/OPEN-NEXT/tldr-ipr/releases/download/v1.0.0/tldr-IPR.pdf))

Over-simply speaking, there are two important types of licencing schemes you can choose between (however, there are many more):

-	Copyleft license
-	Permissive license

----

## Copyleft licenses
Copyleft licenses require that the derivative products have to be released under the same licenses as the original. Common copyleft licenses are for example: GNU General Public License (GPL), Creative Commons Attribution-ShareAlike license, CERN Open Hardware License (OHL), TAPR Open Hardware License.

----

## Permissive license
The permissive license allows that the derivatives from the original do not have to be released under the same license as the original. Common permissive licenses are for example: FreeBSD license, MIT license, Creative Commons Attribution license.

----

**Bonus:** there's also

## Public domain

A third possibility is a public domain. You have all the rights but no obligations. This is less a license than a marking.

---

## [Legal aspects](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#Legal-aspects)

There is another guideline for all the legal matters. 
[Here](https://github.com/OPEN-NEXT/tldr-ipr/releases/download/v1.0.0/tldr-IPR.pdf) you will find the short form for a first overview. 
[Here](https://gitlab.com/OSEGermany/osh-guideline-legal-issues/-/blob/master/Guideline.md) you can find the detailed guideline.


---

# In summary
OSH has great potential, and if you become part of it, the community will grow and so will the progress.

---

## [Contact](https://md.opensourceecology.de/wGS7d4-oQBiuYEpJGws62g?view#Contact) 
---

below
↓

----

Martin Häuer 
https://de.linkedin.com/in/martin-häuer-16a588208
martin.haeuer@ipk.fraunhofer.de

----

Jaime Arredondo – Bold & Open 
https://boldandopen.com
jaime@boldandopen.com

----

Lars Zimmermann
https://larszimmermann.de

----

Frederik Lean Hansen
https://ddc.dk/people/frederik-lean-hansen/
flh@ddc.dk
