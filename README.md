# OSH Guideline | Business Models

This document shall support actors new to the field of open source hardware **to find (or ideate) a sustainable business model** for their case. It's meant to give a general overview of the matter. You can explore the content either in the guideline itself (long version) or in the slides (shortened version).

The document has been created by [BCPro](https://bcpro.de/) (see [authors](#authors) below) in collaboration with [Fraunhofer IPK](https://www.ipk.fraunhofer.de/en/expertise/digital-engineering.html) and the EU-H2020-funded [OPEN!NEXT project](https://opennext.eu/). It is based on open educational resources ([repo](https://gitlab.com/osh-academy/oer-osh-intro), Martin Häuer, [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode)) used in an elective of the [SRH University Berlin](https://www.srh-berlin.de/en/).

## Fast Access to the Content

| Content | View (export) | Edit (source) |
| --- | --- | --- |
| Guideline | [v1.0.0](https://gitlab.com/OSEGermany/osh-guideline-business-models/uploads/82c2a8af233a5fb71676845a258a96bc/OSH-Guideline-Business-Models.pdf) | [Guideline.md](Guideline.md) |
| Slides | [online slide view](https://md.opensourceecology.de/p/QS6ElRVWO#/) | [Slides](Guideline-SLIDES.md)

**General note:**\
Please keep in mind that this document is a draft only and subject to constant change.
In particular, it may contain inaccuracies or defects not yet remedied.

If you notice any inaccuracy or mistake, please feel free to get in
touch with us and contribute!\
The easiest would be to [drop us an issue](https://gitlab.com/OSEGermany/osh-guideline-business-models/-/issues/new).

## Authors

Content in this repository has been created by

- Jonah Schuldt,
- Luca Klein,
- Korbinian Kuhn,
- Jennifer-May Hoffmann

and reviewed (and edited) by Martin Häuer

## License

This work is made available under [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode).
